package science.ceiling.pasokani.exceptions;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

import java.io.*;

public class ErrorHandler {

    /**
     * Method responsible for building and showing the user the exception alert.
     * Upon closing the exception the application will attempt to terminate gracefully.
     *
     * @param message - The message to display in the header of the alert.
     * @param throwable - The throwable from which the stack trace will be fetched.
     */
    private static void showErrorDialog(String message, Throwable throwable, String reportName){
        Platform.runLater(() -> {
            // Build a new alert.
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setGraphic(new ImageView(new Image(ErrorHandler.class.getResourceAsStream("errorIcon.png"))));
            alert.setTitle("Error");
            alert.setHeaderText(message);
            alert.setContentText("A copy of the stack trace has been saved to: "+reportName);

            // Extract the stack trace.
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            throwable.printStackTrace(printWriter);
            printWriter.flush();
            printWriter.close();
            String stackTrace = stringWriter.toString();

            // Build the stack trace elements.
            Label stackTraceLabel = new Label("Exception stack trace:");
            TextArea stackTraceArea = generateExceptionTextArea(stackTrace);
            GridPane stackTracePane = generateExceptionGridPane();

            // Combine the stack trace elements.
            stackTracePane.add(stackTraceLabel, 0, 0);
            stackTracePane.add(stackTraceArea, 0, 1);

            // Attach the stack trace elements to the alert and trigger.
            alert.getDialogPane().setExpandableContent(stackTracePane);
            alert.showAndWait();

            System.exit(0);
        });
    }

    /**
     * A shortcut method of {@link #throwException(String, Throwable)}
     * providing a generic header message.
     * @see #throwException(String, Throwable)
     * @param e - The throwable causing the exception
     */
    public static void throwException(Throwable e) {
        throwException("Application encountered an error: "+e.getMessage(),e);
    }

    /**
     * A shortcut method of {@link #throwException(String, Throwable)}
     * providing a generic runtime exception.
     * @see #throwException(String, Throwable)
     * @param message - The throwable causing the exception
     */
    public static void throwException(String message) {
        throwException(message, new RuntimeException(message));
    }

    /**
     * The method responsible for outputting the exception stack trace to
     * a text file and then calling {@link #showErrorDialog(String, Throwable, String)}
     * to display a matching exception alert.
     *
     * @param message - The message to display in the alert header.
     * @param e - The throwable causing the exception
     */
    private static void throwException(String message, Throwable e) {

        // Create the exception stack trace text file handle.
        File errorReport = new File("crash_" + System.currentTimeMillis() + ".log");
        try {
            if(!errorReport.createNewFile()){
                throw new RuntimeException("Failed to create an error report file.");
            }
        } catch (IOException exception) {
            throw new RuntimeException("Error while trying to create a crash log. at: "+errorReport.getAbsolutePath(), exception);
        }

        try {
            // Write exception to file.
            PrintWriter exceptionWriter = new PrintWriter(errorReport, "UTF-8");
            e.printStackTrace(exceptionWriter);
            exceptionWriter.flush();
            exceptionWriter.close();

            // Show exception alert.
            showErrorDialog(message,e,errorReport.getName());

        } catch (FileNotFoundException | UnsupportedEncodingException printWriterException){
            throw new RuntimeException("Error while trying to write to a crash log at: "+errorReport.getAbsolutePath(), printWriterException);
        }
    }

    private static GridPane generateExceptionGridPane(){
        GridPane foldedPane = new GridPane();
        foldedPane.setMaxWidth(Double.MAX_VALUE);
        foldedPane.setMaxHeight(Double.MAX_VALUE);
        return foldedPane;
    }

    private static TextArea generateExceptionTextArea(String stackTrace){
        TextArea exceptionTA = new TextArea(stackTrace);
        exceptionTA.setMaxHeight(Double.MAX_VALUE);
        exceptionTA.setMaxWidth(Double.MAX_VALUE);
        exceptionTA.setEditable(false);
        exceptionTA.setWrapText(false);
        GridPane.setVgrow(exceptionTA, Priority.ALWAYS);
        GridPane.setHgrow(exceptionTA, Priority.ALWAYS);
        return exceptionTA;
    }
}
