package science.ceiling.pasokani.controllers;

import feign.Feign;
import feign.gson.GsonDecoder;
import javafx.application.Platform;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.paint.Paint;
import science.ceiling.pasokani.PasoKani;
import science.ceiling.pasokani.api.WaniKani;
import science.ceiling.pasokani.exceptions.ErrorHandler;

import java.awt.*;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Timer;
import java.util.TimerTask;

public class PasoController {

    private static final String ERROR_COLOR_VALUE = "#ae0000";
    private static final String SUCCESS_COLOR_VALUE = "#16bc00";

    private static final Timer TIMER = new Timer();

    private static final WaniKani WANIKANI = Feign.builder()
                                                  .errorDecoder(new WaniKani.ApiErrorDecoder())
                                                  .decoder(new GsonDecoder())
                                                  .target(WaniKani.class, "https://api.wanikani.com");

    private static String apiKey = "UNKNOWN";
    private static boolean connectionEstablished = false;

    private TrayIcon trayIcon;

    private ZonedDateTime reviewTime = null;

    @FXML private TextField apiKeyField;
    @FXML private Label configurationStatusLabel;

    private StringProperty configurationStatus;

    @FXML
    public void initialize() {
        configurationStatus = configurationStatusLabel.textProperty();
    }

    public void setTrayIcon(TrayIcon trayIcon) {
        this.trayIcon = trayIcon;
    }

    public void setApiKey() {
        try {
            apiKey = apiKeyField.textProperty().get();
            WaniKani.User newUser = WANIKANI.getUser(apiKey);
            configurationStatusLabel.setTextFill(Paint.valueOf(SUCCESS_COLOR_VALUE));
            configurationStatus.set("Welcome " + newUser.data.username + "!");
            connectionEstablished = true;
            fetchNextReviewTime();
            PasoKani.checkStatus();
        } catch (WaniKani.WaniKaniException waniKaniException) {
            configurationStatusLabel.setTextFill(Paint.valueOf(ERROR_COLOR_VALUE));
            configurationStatus.set(waniKaniException.getMessage());
            connectionEstablished = false;
            checkStatus();
        } catch (Exception genericException) {
            ErrorHandler.throwException(genericException);
            connectionEstablished = false;
        }
    }

    public void fetchNextReviewTime() {
        try {
            if (connectionEstablished) {
                WaniKani.Summary summary = WANIKANI.getSummary(apiKey);
                ZonedDateTime reviewTime = ZonedDateTime.parse(summary.data.next_reviews_at);
                ZonedDateTime currentTime = ZonedDateTime.now().withZoneSameInstant(ZoneOffset.UTC);
                if (!reviewTime.isAfter(currentTime)) {
                    this.reviewTime = null;
                    if (PasoKani.notificationsActive) {
                        trayIcon.displayMessage("Review Time!", "You have pending reviews!", TrayIcon.MessageType.INFO);
                    }
                    TIMER.purge();
                    TIMER.schedule(new CheckStatusTask(), 300 * 1000); // Check back in 5 minutes.
                } else {
                    this.reviewTime = reviewTime;
                    TIMER.purge();
                    TIMER.schedule(new CheckStatusTask(), calculateMillisecondsBeforeReviews());
                }
            }
            checkStatus();
        } catch (WaniKani.WaniKaniException waniKaniException) {
            configurationStatusLabel.setTextFill(Paint.valueOf(ERROR_COLOR_VALUE));
            configurationStatus.set(waniKaniException.getMessage());
        } catch (Exception genericException) {
            ErrorHandler.throwException(genericException);
        }
    }

    private long calculateMillisecondsBeforeReviews() {
        return ChronoUnit.MILLIS.between(ZonedDateTime.now().withZoneSameInstant(ZoneOffset.UTC), reviewTime) + 1000;
    }

    public String checkStatus() {
        if (!PasoKani.notificationsActive) {
            trayIcon.setToolTip("PasoKani is napping. Zzz...");
        }
        if (connectionEstablished) {
            if (reviewTime == null) {
                trayIcon.setToolTip("Reviews pending!");
                return "You have pending reviews!";
            } else {
                trayIcon.setToolTip("Reviews available at: " + calculateTimeOfReviews() + "!");
                return calculateReadableTimeUntilReviews();
            }
        } else {
            return "PasoKani is enjoying his free time.";
        }
    }

    private String calculateTimeOfReviews() {
        return reviewTime.withZoneSameInstant(ZonedDateTime.now().getZone()).toLocalTime().format(DateTimeFormatter.ofPattern("HH:mm"));
    }

    private String calculateReadableTimeUntilReviews() {
        ZonedDateTime currentTime = ZonedDateTime.now().withZoneSameInstant(ZoneOffset.UTC);
        long minutesBeforeReview = ChronoUnit.MINUTES.between(currentTime, reviewTime);
        long hoursBeforeReview = 0;
        long daysBeforeReview = 0;

        if (minutesBeforeReview > 59) {
            hoursBeforeReview = minutesBeforeReview / 60;
            minutesBeforeReview = minutesBeforeReview % 60;
            if (hoursBeforeReview > 23) {
                daysBeforeReview = hoursBeforeReview / 24;
                hoursBeforeReview = hoursBeforeReview % 24;
            }
        }

        StringBuilder messageBuilder = new StringBuilder();
        boolean buildStarted = false;
        if (daysBeforeReview > 0) {
            if (daysBeforeReview == 1) {
                messageBuilder.append("1 day");
            } else {
                messageBuilder.append(daysBeforeReview + " days");
            }
            buildStarted = true;
        }
        if (hoursBeforeReview > 0) {
            if (buildStarted) {
                messageBuilder.append(", ");
            }
            if (hoursBeforeReview == 1) {
                messageBuilder.append("1 hour");
            } else {
                messageBuilder.append(hoursBeforeReview + " hours");
            }
            buildStarted = true;
        }
        if (minutesBeforeReview > 0) {
            if (buildStarted) {
                messageBuilder.append(", ");
            }
            if (minutesBeforeReview == 1) {
                messageBuilder.append("1 minute");
            } else {
                messageBuilder.append(minutesBeforeReview + " minutes");
            }
        }
        messageBuilder.append(" before reviews become available.");
        return messageBuilder.toString();
    }

    class CheckStatusTask extends TimerTask {
        @Override
        public void run() {
            Platform.runLater(PasoController.this::fetchNextReviewTime);
        }
    }
}
