package science.ceiling.pasokani;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import science.ceiling.pasokani.controllers.PasoController;
import science.ceiling.pasokani.exceptions.ErrorHandler;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class PasoKani extends Application {

    private static TrayIcon trayIcon;
    private static PasoController controller;

    public static boolean notificationsActive = true;

    @Override
    public void start(Stage stage) throws Exception{
        // Tray Icon Support
        Platform.setImplicitExit(false);
        suppressTraditionalWindowClose(stage);
        configureContextMenu(stage);

        // JavaFX Configuration
        FXMLLoader loader = new FXMLLoader(getClass().getResource("pasokani.fxml"));
        Parent root = loader.load();
        controller = loader.getController();
        controller.setTrayIcon(trayIcon);
        stage.setTitle("PasoKani Settings");
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.getIcons().add(new javafx.scene.image.Image(getClass().getResourceAsStream("pasokani.png")));
        stage.show();
        stage.toFront();
    }

    private void suppressTraditionalWindowClose(final Stage stage) {
        stage.setOnCloseRequest((event) -> hideWindow(stage));
    }

    private void hideWindow(Stage stage) {
        Platform.runLater(() -> {
            if (SystemTray.isSupported()) {
                stage.hide();
                showTrayTooltip();
            } else {
                System.exit(0);
            }
        });
    }

    private void showTrayTooltip() {
        if (trayIcon != null && notificationsActive) {
            trayIcon.displayMessage("PasoKani is still here for you.", "PasoKani is chilling in your system tray. Let him know if you wish to send him home.", TrayIcon.MessageType.INFO);
        }
    }

    private void configureContextMenu(final Stage stage) throws IOException, AWTException {
        if (SystemTray.isSupported()) {
            SystemTray systemTray = SystemTray.getSystemTray();
            Image trayIconImage = ImageIO.read(getClass().getResourceAsStream("pasoicon.png"));

            ActionListener settingsListener = (event) -> showStage(stage);
            ActionListener notificationListener = (event) -> toggleNotifications();
            ActionListener statusCheckListener = (event) -> checkStatus();
            ActionListener exitListener = (event) -> System.exit(0);

            PopupMenu contextMenu = new PopupMenu();
            MenuItem settingsOption = new MenuItem("Configure");
            MenuItem notificationOption = new MenuItem("Sleep");
            MenuItem statusCheckOption = new MenuItem("Check");
            MenuItem exitOption = new MenuItem("Send Home");

            settingsOption.addActionListener(settingsListener);
            notificationOption.addActionListener(notificationListener);
            statusCheckOption.addActionListener(statusCheckListener);
            exitOption.addActionListener(exitListener);

            contextMenu.add(settingsOption);
            contextMenu.add(notificationOption);
            contextMenu.add(statusCheckOption);
            contextMenu.add(exitOption);

            trayIcon = new TrayIcon(trayIconImage, "PasoKani is enjoying his free time.", contextMenu);
            trayIcon.setImageAutoSize(true);
            trayIcon.addActionListener(settingsListener);

            systemTray.add(trayIcon);
        } else {
            ErrorHandler.throwException("Unable to hook onto the operating system SystemTray.");
        }
    }

    public static void checkStatus() {
        if (notificationsActive) {
            controller.fetchNextReviewTime();
            trayIcon.displayMessage("Status Check at: " + LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm")), controller.checkStatus(), TrayIcon.MessageType.INFO);
        }
    }

    private void showStage(Stage stage) {
        if (stage.isShowing()) {
            Platform.runLater(stage::toFront);
            Platform.runLater(stage::requestFocus);
        } else {
            Platform.runLater(stage::show);
        }
    }

    private void toggleNotifications() {
        if (notificationsActive) {
            trayIcon.displayMessage("Nap Time", "PasoKani is taking a nap. Wake him up when you need him.", TrayIcon.MessageType.INFO);
            trayIcon.getPopupMenu().getItem(1).setLabel("Wake Up");
            trayIcon.getPopupMenu().getItem(2).setEnabled(false);
            notificationsActive = false;
            controller.checkStatus();
        } else {
            trayIcon.displayMessage("Yawn", "PasoKani is awake and ready.", TrayIcon.MessageType.INFO);
            trayIcon.getPopupMenu().getItem(1).setLabel("Sleep");
            trayIcon.getPopupMenu().getItem(2).setEnabled(true);
            notificationsActive = true;
            controller.fetchNextReviewTime();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
