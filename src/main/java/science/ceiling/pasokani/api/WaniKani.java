package science.ceiling.pasokani.api;

import feign.Headers;
import feign.Param;
import feign.RequestLine;
import feign.Response;
import feign.codec.ErrorDecoder;

public interface WaniKani {

    int UNAUTHORIZED = 401;
    int TOO_MANY_REQUESTS = 429;

    @RequestLine("GET /v2/summary")
    @Headers("Authorization: Bearer {token}")
    Summary getSummary(@Param("token") String userToken) throws WaniKaniException;

    @RequestLine("GET /v2/user")
    @Headers("Authorization: Bearer {token}")
    User getUser(@Param("token") String userToken) throws WaniKaniException;

    class Summary {
        public SummaryData data;
        public class SummaryData {
            public String next_reviews_at;
        }
    }

    class User {
        public UserData data;
        public class UserData {
            public String username;
        }
    }

    class ApiErrorDecoder implements ErrorDecoder {
        public Exception decode(String methodKey, Response response) {
            if (response.status() == UNAUTHORIZED) {
                return new WaniKaniKeyUnrecognizedException();
            }
            if (response.status() == TOO_MANY_REQUESTS) {
                return new WaniKaniTooManyRequestsException();
            }
            return new WaniKaniGenericApiException(response.status());
        }
    }

    class WaniKaniException extends Exception {
        WaniKaniException(String message) {
            super(message);
        }
    }

    class WaniKaniKeyUnrecognizedException extends WaniKaniException {
        WaniKaniKeyUnrecognizedException() {
            super("ERROR - KEY VALUE INVALID");
        }
    }

    class WaniKaniTooManyRequestsException extends WaniKaniException {
        WaniKaniTooManyRequestsException() {
            super("ERROR - TOO MANY REQUESTS");
        }
    }

    class WaniKaniGenericApiException extends WaniKaniException {
        WaniKaniGenericApiException(int code) {
            super("ERROR - CODE: " + code);
        }
    }
}
